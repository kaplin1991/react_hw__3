import React from 'react'

import PropTypes from 'prop-types'

const Toggler = ( { name, children, action, activeState  } ) => {
    
    return(
        <>
            <label data-name={name}>{name}
            {
                React.Children.count( children ) > 0 &&
                React.Children.map( children, ( child ) => {
                    if( React.isValidElement ( child ) ){
                        return React.cloneElement(
                            child,
                            {
                                activeState: child.props.value === activeState,
                                action: action( child.props.value ),
                                name: child.props.value
                            }
                        );
                    }
                })
            } 
            </label> 
        </>
    )
} 

Toggler.propTypes = {
	name: PropTypes.string.isRequired,
	action: PropTypes.func.isRequired,
	active: PropTypes.string,
	child: PropTypes.element
}

export default Toggler;