/*

	Задание 1:

	Написать кастомный компонент переключалки, который можно будет ре-использовать в форме.
	По примеру того, что мы разобрали в аудитории.

	Результатом должен стать компонент, который принимает в себя:
	- параметры переключалки отдельными дочерними компонентами
	- активное состояние
	- name
	- action который изменяет стейт родителя
	+ бонус, action должен быть универсальным для нескольких переключалок.

	У компонента должна быть проверка PropTypes.
	- name: обязательное поле, строка
	- action: обязательное поле, функция
	- activeState: не обязательное поле, строка
	- children, не обязательное поле, реакт-компонент


	Тест: Добавить 2 элемента Toggler которые будут менять стейт родителя.
		Пусть это будут поля:
		gender > male,female
		layout > left, center, right, baseline

*/

import React, { Component } from 'react'

/* import { Toggler, TogglerItem } from './Task1' */
import Toggler from './Toggler'
import TogglerItem from './TogglerItem'
 



class Task1 extends Component { 
 
	state = {
		Gender: '',
		layout: ''
	} 

	changeToggler = (value) => (event) => {
		const name = event.target.parentElement.dataset.name;
		this.setState({ 
			[name]: value 
		}) 
	}
 
    render = () => {
		const { Gender, layout } = this.state;

        return(
            <> 
                <Toggler name='Gender' action={this.changeToggler} activeState={Gender}>
            		{ undefined }
                    <TogglerItem value='Toggler 1'> </TogglerItem>
                    <TogglerItem value='Toggler 2'> </TogglerItem>
                </Toggler> 
				<br></br>
                <Toggler name='layout' action={this.changeToggler} activeState={layout}>
            		{ undefined }
                    <TogglerItem value='left'> </TogglerItem>
                    <TogglerItem value='center'> </TogglerItem>
                    <TogglerItem value='right'> </TogglerItem>
                    <TogglerItem value='baseline'> </TogglerItem>
                </Toggler> 
            </>
        )
    }
}


export default Task1;