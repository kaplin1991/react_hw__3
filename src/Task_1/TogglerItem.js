import React from 'react'


const TogglerItem = ({ value, activeState, action, name }) => {
 
    return(
        <>
            <button
                name={name}
                type="button"
                style={{ background: activeState ? "yellow" : "transparent"}}
                onClick={ action }
            >
                { value }
            </button>
        </>
    )
} 

export default TogglerItem;
 