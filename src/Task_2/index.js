import React, { Component } from 'react'

/* import Input from './Input' */

import { Input } from './Input'

class Task2 extends Component {

    state = {
        value: ''
    }

    onChangeHandler = ( event ) => {   
        this.setState({ value : event.target.inputValue })
    }

    render = () => {
        const { value } = this.state;
        const { onChangeHandler } = this;

        return (
            <>
                <Input
                    type='text'
                    name='input'
                    placeholder='placeholder'
                    value={value}
                    handler={onChangeHandler}
                >Ввод
                </Input>
            </>
        )
    }
}
 

export default Task2;