import React from 'react'

import PropTypes from 'prop-types'

export const Input = ({ name, type, placeholder, value, handler, children }) => {  
 
    return (     
            <label>
                <div>{children}:</div>

                <input
                    type={type}
                    name={name}
                    placeholder={placeholder}
                    value={value}
                    onChange={handler}
                />
            </label> 
    )
}
/*
const COMPONENT_TYPES = [ 'text', 'password', 'number'  ];

Input.propTypes = {
    type: PropTypes.oneOf(COMPONENT_TYPES).isRequired,
    placeholder: PropTypes.string,
    value: PropTypes.any,
    handler: PropTypes.func.isRequired,
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number
 
}*/
 
export default Input;