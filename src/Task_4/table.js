import React, {Component} from 'react'

import Input from '../Task_2/Input'

const create = function(tag){
    return document.createElement(tag);
}


class Table extends Component{
  
    state = {
        value: ''
    }

    /* onChangeHandler = ( event ) => {   
        this.setState({ value : event.target.inputValue })
    } */
    
    componentDidMount(){
        let customTable = document.querySelector('.customTable'); 

        const createCustomTable = function ( trCount, tdCount, v, f ) {

            Array.from({ length: trCount },
                ( ) => {

                    let tr = create('tr');
                    customTable.append(tr);

                    Array.from({ length: tdCount },
                        ( ) => { 
                            let td = create('td');
                            td.innerHTML = <Input
                                type='text'
                                name='input'
                                placeholder='placeholder'
                                value={v}
                                handler={f}
                                ></Input> ; 
                            tr.append(td);
                        }
                    )

                }
            )
        }
        createCustomTable( 3, 3, this.state.v ) 
    }

    render = () => { 

        return (
            <table className='customTable'>
            </table>
        )
    }
}
 

export default Table;