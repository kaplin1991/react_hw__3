import React, { Component } from 'react'

import Toggler from '../Task_1/Toggler'
import TogglerItem from '../Task_1/TogglerItem'
import Input from '../Task_2/Input'
import Submit from './Submit'

class Task3 extends Component {

    state = {
        Gender: '',
        layout: '',
        name: '',
        pass: '',
        age: '',
        lang: ''
    }

    changeToggler = (value) => (event) => {
        const name = event.target.parentElement.dataset.name;
        this.setState({
            [name]: value
        })
    } 
    
    onChangeHandler = ( event ) => {   
        console.dir(event.target.value)
        const name = event.target.name;
        this.setState({ [name] : event.target.value })
    }

    submitAction = (event) => {
        event.preventDefault()
        console.log(this.state)
    }

    render = () => {

        const { Gender, layout, name, pass, age, lang } = this.state;
        const { onChangeHandler, changeToggler, submitAction } = this;


        return (
            <>
                <form>
                    <Input
                        type='text'
                        name='name'
                        placeholder='Пётр'
                        value={name}
                        handler={onChangeHandler}
                    >Имя
                    </Input>
                    <Input
                        type='password'
                        name='pass'
                        placeholder='password'
                        value={pass}
                        handler={onChangeHandler}
                    >Пароль
                    </Input>
                    <br></br>
                    <Toggler name='Gender' action={changeToggler} activeState={Gender}>
                        <TogglerItem value='Male'> </TogglerItem>
                        <TogglerItem value='Female'> </TogglerItem>
                    </Toggler>
                    <Input
                        type='number'
                        name='age'
                        placeholder='age'
                        value={age}
                        handler={onChangeHandler}
                    >Возраст
                    </Input> 
                    <br></br>
                    <Toggler name='layout' action={changeToggler} activeState={layout}>
                        <TogglerItem value='left'> </TogglerItem>
                        <TogglerItem value='center'> </TogglerItem>
                        <TogglerItem value='right'> </TogglerItem>
                        <TogglerItem value='baseline'> </TogglerItem>
                    </Toggler>
                    <Input
                        type='text'
                        name='lang' 
                        value={lang}
                        handler={onChangeHandler}
                    >Язык
                    </Input>
                    <Submit
                        type='submit'
                        value='Send'
                        action={submitAction}
                    ></Submit>
                </form>
            </>
        )
    }
}

export default Task3;